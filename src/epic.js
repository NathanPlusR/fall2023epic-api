/* eslint-disable max-len */
// eslint-disable-next-line strict
"use strict";

(function(){

    const EpicApplication = {
        images: null,
        collection: null,
        dateUsed: null,
        dateCache : {}
    };

    const imageCache = {};

    document.addEventListener("DOMContentLoaded", function(){
        const ImageTypeSelect = document.getElementById("type");
        EpicApplication["collection"] = ImageTypeSelect.value;
        updateDate(EpicApplication["collection"]);

        function updateDate(collection){
            if (!EpicApplication["dateCache"][collection]){
                fetch(`https://epic.gsfc.nasa.gov/api/${collection}/all`, {})
                    .then( response => { 
                        if( !response.ok ) { 
                            throw new Error("Not 2xx response", {cause: response});
                        }
                        return response.json();
                    })
                    .then( obj => {
                        EpicApplication["dateCache"][collection] = getMostRecentDate(obj);
                        document.getElementById("date").max = EpicApplication["dateCache"][collection];
                    })
                    .catch( err => {
                        console.error("3)Error:", err);
                        return null;
                    });
            } else {
                document.getElementById("date").max = EpicApplication["dateCache"][collection];
            }
        }

        function getMostRecentDate(dateObjectArray){
            return dateObjectArray.map(element => element.date)
                .reduce( (acc, cur) => {
                    if (acc < cur){
                        return cur;
                    } else{
                        return acc;
                    }
                }, "0-0-0");
        }

        function updateImageSelectionUI(){
            let i = 0;
            const ul = document.getElementById("image-menu");
            ul.innerText = "";

            if (EpicApplication["images"].length === 0){
                ul.textContent = "Error! No images found!";
            } else {
                EpicApplication["images"].forEach(element => {
                    const li = document.getElementById("image-menu-item").content
                        .querySelector("li").cloneNode(true);
                    li.querySelector("span").textContent = element.date;
                    li.setAttribute("data-image-index", i);
                    i++;
                    ul.appendChild(li);
                });
            }
        }

        document.getElementById("type").addEventListener("change", function(e){
            updateDate(e.target.value);
        });

        document.getElementById("image-menu")
            .addEventListener("click", function(e){
                const index = e.target.parentElement.getAttribute("data-image-index");
                if (e.target.tagName === "SPAN") {
                    const imageURL = Object.values({
                        Collection: EpicApplication["collection"],
                        Date: EpicApplication["dateUsed"].replaceAll("-", "/"),
                        "Image Type": "jpg",
                        "File Name": EpicApplication["images"][index].image,
                    }).reduce( (acc, cur) => acc + "/" + cur) + ".jpg";

                    document.getElementById("earth-image").src = 
                        `https://epic.gsfc.nasa.gov/archive/${imageURL}`;
                    document.getElementById("earth-image-date").textContent = 
                        EpicApplication["images"][index].date;
                    document.getElementById("earth-image-title").textContent = 
                        EpicApplication["images"][index].caption;
                }
            });

        document.getElementById("request_form")
            .addEventListener("submit", function(e) {
                e.preventDefault();

                const searchParams = {
                    Collection: document.getElementById("type").value,
                    Date: document.getElementById("date").value,
                };

                EpicApplication["collection"] = searchParams.Collection;
                EpicApplication["dateUsed"] = searchParams.Date;

                if (!imageCache[EpicApplication["collection"]]){
                    imageCache[EpicApplication["collection"]] = new Map();
                }

                if (!imageCache[EpicApplication["collection"]].get(EpicApplication["dateUsed"])){
                    fetch(
                        `https://epic.gsfc.nasa.gov/api/${EpicApplication["collection"]}/date/${EpicApplication["dateUsed"]}`, {})
                        .then( response => { 
                            if( !response.ok ) { 
                                throw new Error("Not 2xx response", {cause: response});
                            }
                            
                            return response.json();
                        })
                        .then( obj => {
                            imageCache[EpicApplication["collection"]].set(EpicApplication["dateUsed"], obj);
                            EpicApplication["images"] = imageCache[EpicApplication["collection"]].get(EpicApplication["dateUsed"]);
                            updateImageSelectionUI();
                        })
                        .catch( err => {
                            console.error("3)Error:", err);
                        });
                } else {
                    EpicApplication["images"] = imageCache[EpicApplication["collection"]].get(EpicApplication["dateUsed"]);
                    updateImageSelectionUI();
                }
            });
    });
})();